#!/bin/bash -xe
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
    #
    cd /home/ec2-user/

    # dependencies
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
    . /.nvm/nvm.sh
    nvm install 8.10.0

    # env variables
    DATABASE_PWD=''

    # download build
    mkdir name-list
    cd name-list
    aws s3 cp s3://zhiping-name-list-build/build.zip ./
    unzip ./build.zip -d ./
    rm -r ./node_modules/.bin/
    npm install pm2

    # start
    npm run start:prod
