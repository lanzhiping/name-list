import React, { useEffect, useState } from 'react';
import Axios from 'axios';

const Item = ({ children }) => {
    const style = {
        display: 'inline-block',
        padding: '10px 20px',
        minWidth: '150px'
    };
    return (<span style={style}>{children}</span>);
};
const ListHeader = ({ children }) => {
    const style = { borderBottom: '1px solid' };
    return (<div style={style}>{children}</div>);
};
const ListFooter = ({ children, show }) => {
    const style = { borderTop: '1px solid', display: show ? 'block' : 'none' };
    return (<div style={style}>{children}</div>);
};

const IndexPage = ({ nameList }) => {
    const [nameListState, setNameList] = useState(nameList);
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const [editingId, setEditingId] = useState('');
    const [editingName, setEditingName] = useState('');
    const [editingAge, setEditingAge] = useState('');
    const [addName, setAddName] = useState('');
    const [addAge, setAddAge] = useState('');

    const onPreviousClick = () => {
        setPage(Math.max(1, page - 1));
    };
    const onNextClick = () => {
        setPage(page + 1);
    };

    const onSaveClick = () => {
        const payload = {
            name: editingName,
            age: editingAge
        };
        Axios.put(`/api/names/${editingId}`, payload)
            .then(() => {
                loadNameList(page);
                setEditingId('');
                setEditingName('');
                setEditingAge('');
            });
    };

    const onEditClick = (event) => {
        const id = event.target.getAttribute('data-id');
        setEditingId(id);
    };

    const onNameChange = (event) => setEditingName(event.target.value);

    const onAgeChange = (event) => setEditingAge(event.target.value);

    const onRemoveClick = (event) => {
        const name = event.target.getAttribute('data-name');
        const id = event.target.getAttribute('data-id');
        if (window.confirm(`Remove ${name} ?`)) {
            Axios.delete(`/api/names/${id}`)
                .then(() => loadNameList(page));
        }
    };

    const onAddNameChange = (event) => setAddName(event.target.value);

    const onAddAgeChange = (event) => setAddAge(event.target.value);

    const onAddClick = () => {
        const payload = [{
            name: addName,
            age: addAge
        }];
        Axios.post('/api/names', payload)
            .then(() => {
                loadNameList(page);
                setAddName('');
                setAddAge('');
            })
    };

    const loadNameList = (page) => Axios
        .get(`/api/names?page=${page}`)
        .then(res => {
            setNameList(res.data);
            setLoading(false);
        });

    const renderPerson = ({ name, id, age }) => (editingId === id) ? (
        <div key={id}>
            <Item><input defaultValue={name} onInput={onNameChange}/></Item>
            <Item><input defaultValue={age} onInput={onAgeChange}/></Item>
            <Item><button data-id={id} onClick={onSaveClick}>Save</button></Item>
        </div>
    ) :(
        <div key={id}>
            <Item>{name}</Item>
            <Item>{age}</Item>
            <Item><button data-id={id} onClick={onEditClick}>Edit</button></Item>
            <Item><button data-id={id} data-name={name} onClick={onRemoveClick}>Remove</button></Item>
        </div>
    );

    useEffect(() => {
        setLoading(true);
        setNameList([]);
        loadNameList(page);
    }, [page]);

    return (
        <div>
            <h1>Name List Page {page}</h1>
            <ListHeader>
                <Item>Name</Item>
                <Item>Age</Item>

            </ListHeader>
            <div>
                <Item><input onInput={onAddNameChange}/></Item>
                <Item><input onInput={onAddAgeChange}/></Item>
                <Item><button onClick={onAddClick}>Add Name</button></Item>
            </div>
            <div>{loading && <Item>Loading...</Item>}</div>
            <div>{nameListState.map(renderPerson)}</div>
            <ListFooter show={nameListState.length > 0}>
                <Item><button onClick={onPreviousClick}>Previous</button></Item>
                <Item><button onClick={onNextClick}>Next</button></Item>
            </ListFooter>
        </div>
    );
};

IndexPage.getInitialProps = async () => ({ nameList: [] });

export default IndexPage;