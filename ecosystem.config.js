const packageInfo = require('./package.json');
const path = require('path');

module.exports = {
    apps: [{
        name: packageInfo.name,
        script: packageInfo.main,
        exec_mode: 'cluster',
        instances: '2',
        max_memory_restart: '500M',
        max_restarts: 10,
        listen_timeout: 8000,
        combine_logs: true,
        log_type: 'json',
        error_file: path.resolve(__dirname, `logs/error_${new Date().toLocaleDateString('zh')}.log`),
        out_file: path.resolve(__dirname, `logs/out_${new Date().toLocaleDateString('zh')}.log`),
        env: {
            'NODE_ENV': 'production',
            'PORT': 8080
        }
    }]
};
