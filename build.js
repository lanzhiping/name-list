const zipdir = require('zip-dir');
const fs = require('fs');
const AWS = require('aws-sdk');

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

const uploadFile = () => {
    fs.readFile('./build.zip', (err, data) => {
        if (err) throw err;

        const base64data = new Buffer(data, 'binary');

        s3.putObject({
            Bucket: 'zhiping-name-list-build',
            Key: 'build.zip',
            Body: base64data
        }, function (s3Err, data) {
            if (s3Err) throw s3Err

            console.log(`File uploaded successfully at ${data.Location}`)
        });
    });
};



zipdir('./', { saveTo: './build.zip' }, (err) => {
    if (err) throw err;
    console.log('zip build done');
    uploadFile();
});