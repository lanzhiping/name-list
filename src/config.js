const { NODE_ENV, PORT, DATABASE_PWD } = process.env;

module.exports = {
    isDev: NODE_ENV !== 'production',
    port: PORT || 8080,
    dbConfig: {
        host: 'name-list-db.cglvrjdzrmrm.ap-southeast-1.rds.amazonaws.com',
        port: 3306,
        database: 'name_list',
        user: 'name_list_db',
        password: DATABASE_PWD || ''
    }
}