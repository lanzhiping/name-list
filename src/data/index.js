const mysql = require('mysql');
const { dbConfig } = require('../config');

const connection = mysql.createConnection(dbConfig);

connection.connect((err) => {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }
    console.log('connected as id ' + connection.threadId);
});

const execQuery = (query, isLog) => new Promise((resolve, reject) => {
    isLog && console.log('exec query:', query);
    connection.query(
        query,
        (error, results) => error ? reject(error) : resolve(results)
    )
});

const getNameList = (query) => {
    const current = (query.page - 1) * 10;

    return execQuery(`select * from \`name_list\` limit ${current},10`);
};

const createNameRow = (nameList, isLog) => {
    if (!nameList.length) {
        return new Error('name list is empty or not a list, can not create name rows');
    }

    return execQuery(
        mysql.format('insert into `name_list`(id, name, age) values ?', [nameList]),
        isLog
    );
};

const updateNameList = ({ id, name, age }) => {
    if (!id) {
        throw new Error('id is not defined, can not update the name list');
    }

    const nameUpdate = name ? `name = ${name},` : '';
    const ageUpdate = age ? `age = ${age}` : '';

    return execQuery(`update \`name_list\` set ${nameUpdate}${ageUpdate} where id='${id}'`);
};

const deleteNameList = (id) => {
    return execQuery(`delete from \`name_list\` where id='${id}'`);
};

process.on('exit', () => {
    closeConnection();
    console.log('close db connect.');
});

const closeConnection = () => connection.end();

module.exports = {
    execQuery,
    getNameList,
    updateNameList,
    createNameRow,
    deleteNameList,
    closeConnection
};