const path = require('path');
const lineReader = require('line-reader');
const dataService = require('./index');

const checkTable = async () => dataService.execQuery(`
    select 1 from name_list.name_list limit 1
`);

const createTable = async () => dataService.execQuery(`
    create table if not exists name_list.name_list (
        \`id\` varchar(64) not null,
        \`name\` text not null,
        \`age\` int(11) not null,
        primary key (\`id\`)
    )
`)

const initTable = () => new Promise((resolve) => {
    const csvFile = path.resolve(__dirname, './name-list.csv');
    const promises = [];
    let buffer = [];
    let count = 0;

    lineReader.eachLine(csvFile, (line, isLast) => {
        if (count === 0) {
            console.log('start reading file');
            count = count + 1;
            return;
        }

        const nameRow =  line.split(',');
        buffer.push(nameRow);

        if (buffer.length === 1000 || isLast) {
            promises.push(dataService.createNameRow(buffer, false));
            buffer = [];
        }

        if (isLast) {
            Promise.all(promises).then(resolve);
        }
    })
});

(async () => {
    try {
        await checkTable();
        console.log('table name_list ready');
    } catch (e) {
        await createTable();
        await initTable();
    } finally {
        dataService.closeConnection();
    }
})()
