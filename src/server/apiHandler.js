const express = require('express');
const uuid = require('uuid/v1');
const dataService = require('../data');

const getNameList = (req, res) => dataService
    .getNameList(req.query)
    .then((results) => res.json(results));

const updateNameList = (req, res) => dataService
    .updateNameList({ ...req.body, id: req.params.id })
    .then(() => res.end('updated'));

const addNameList = (req, res) => dataService
    .createNameRow(req.body.map(({ name, age }) => [uuid(), name, age]))
    .then(() => res.end('added'));

const deleteNameList = (req, res) => dataService
    .deleteNameList(req.params.id)
    .then(() => res.end('removed'))

module.exports = express.Router()
    .get('/names', getNameList)
    .post('/names', addNameList)
    .put('/names/:id', updateNameList)
    .delete('/names/:id', deleteNameList);
