const { parse } = require('url');
const next = require('next');
const express = require('express');
const config = require('../config');
const apiHandler = require('./apiHandler');

const app = next({ dev: config.isDev });
const pageHandler = app.getRequestHandler();

app.prepare()
    .then(() => {
        express()
            .use(express.json())
            .use('/api', apiHandler)
            .use((req, res) => pageHandler(req, res, parse(req.url, true)))
            .listen(config.port, err => {
                if (err) throw err;
                console.log(`> Ready on http://localhost:${config.port}`);
            });
    });
